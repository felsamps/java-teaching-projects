
package exemplo3;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PreenchedorVetor extends Thread {
    private VetorSimples vs;
    private int valorInicial;
    
    public PreenchedorVetor(VetorSimples vs, int valorInicial) {
        this.vs = vs;
        this.valorInicial = valorInicial;
    }
    
    public void run() {
        for (int i = 0; i < 3; i++) {
            vs.adiciona(valorInicial + i);
        }
    }
}
