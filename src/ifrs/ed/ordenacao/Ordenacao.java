package ifrs.ed.ordenacao;

import ifrs.utils.MedidorTempo;
import java.util.Arrays;
import java.util.Random;



public class Ordenacao {
    public static int TAMANHO = 1000;
    public static int[] aleat1, aleat2, aleat3;
    public static int[] ordenado;
    public static int[] dec1, dec2, dec3;
    
    public static void main(String[] args) {
        
        
        geraVetores();
        
        /* Imprime vetores - para debug
        imprimeVetor(aleat1);
        imprimeVetor(aleat2);
        imprimeVetor(aleat3);
        imprimeVetor(ordenado);
        imprimeVetor(dec1);
        imprimeVetor(dec2);
        imprimeVetor(dec3);	*/
        
        
        System.out.println("Algoritmo\t Repetições\t Trocas");
        
        /* BUBBLE SORT */
        System.out.print("BubbleSort (Dados Aleatórios)\t ");
        MedidorTempo mBubbleAleat = new MedidorTempo();
        mBubbleAleat.inicia();
        bubbleSort(aleat1);
        mBubbleAleat.termina();
        
        System.out.print("BubbleSort (Dados Ordenados)\t ");
        MedidorTempo mBubbleOrd = new MedidorTempo();
        mBubbleOrd.inicia();
        bubbleSort(ordenado);
        mBubbleOrd.termina();
        
        System.out.print("BubbleSort (Dados Ordem Inversa)\t ");
        MedidorTempo mBubbleDec = new MedidorTempo();
        mBubbleDec.inicia();
        bubbleSort(dec1);
        mBubbleDec.termina();
        
        /* BUBBLE SORT OTIMIZADO */
        System.out.print("BubbleSortOtim (Dados Aleatórios)\t ");
        MedidorTempo mBubbleOtAleat = new MedidorTempo();
        mBubbleOtAleat.inicia();
        bubbleSortOtim(aleat2);
        mBubbleOtAleat.termina();
        
        System.out.print("BubbleSortOtim (Dados Ordenados)\t ");
        MedidorTempo mBubbleOtOrd = new MedidorTempo();
        mBubbleOtOrd.inicia();
        bubbleSortOtim(ordenado);
        mBubbleOtOrd.termina();
        
        System.out.print("BubbleSort (Dados Ordem Inversa)\t ");
        MedidorTempo mBubbleOtDec = new MedidorTempo();
        mBubbleOtDec.inicia();
        bubbleSortOtim(dec2);
        mBubbleOtDec.termina();
        
        
        /* SELECTION SORT */
        System.out.print("SelectionSort (Dados Aleatórios)\t ");
        MedidorTempo mSelectionAleat = new MedidorTempo();
        mSelectionAleat.inicia();
        selectionSort(aleat3);
        mSelectionAleat.termina();
        
        System.out.print("SelectionSort (Dados Ordenados)\t ");
        MedidorTempo mSelectionOrd = new MedidorTempo();
        mSelectionOrd.inicia();
        selectionSort(ordenado);
        mSelectionOrd.termina();
        
        System.out.print("SelectionSort (Dados Ordem Inversa)\t ");
        MedidorTempo mSelectionDec = new MedidorTempo();
        mSelectionDec.inicia();
        selectionSort(dec3);
        mSelectionDec.termina();
        
        System.out.println("\nAlgoritmo\tAleatórios\tOrdenados\tOrdem Inversa");
        
        System.out.println("BubbleSort\t" + mBubbleAleat.getTimeInMilisec() +
                "\t" + mBubbleOrd.getTimeInMilisec() +
                "\t" + mBubbleDec.getTimeInMilisec());
        
        System.out.println("BubbleSortOtim\t" + mBubbleOtAleat.getTimeInMilisec() +
                "\t" + mBubbleOtOrd.getTimeInMilisec() +
                "\t" + mBubbleOtDec.getTimeInMilisec());
        
        System.out.println("SelectionSort\t" + mSelectionAleat.getTimeInMilisec() +
                "\t" + mSelectionOrd.getTimeInMilisec() +
                "\t" + mSelectionDec.getTimeInMilisec());
    }
    
    
    public static void bubbleSort(int[] V) {
        int T = V.length;
        int rep = 0, troc = 0;
        for(int n = T-1; n > 0; n--) {
            for(int i = 0; i < n; i++) {
                rep++;
                if(V[i] > V[i+1]) {
                    int aux = V[i];
                    V[i] = V[i+1];
                    V[i+1] = aux;
                    troc++;
                }
            }
        }
        System.out.println(rep + "\t " + troc);
    }
    
    public static void bubbleSortOtim(int[] V) {
        int T = V.length;
        int rep = 0, troc = 0;
        for(int n = T-1; n > 0; n--) {
            boolean troca = false;
            for(int i = 0; i < n; i++) {
                rep++;
                if(V[i] > V[i+1]) {
                    int aux = V[i];
                    V[i] = V[i+1];
                    V[i+1] = aux;
                    troca = true;
                    
                    troc++;
                }
            }
            if(!troca) {
                break;
            }
        }
        System.out.println(rep + "\t" + troc);
    }
    
    public static void selectionSort(int[] V) {
        int T = V.length;
        int rep = 0, troc = 0;
        for(int eleito = 0; eleito < T-1; eleito++) {
            /* busca do menor elemento à direita do eleito */
            int menor = Integer.MAX_VALUE;
            int posMenor = -1;
            for(int i = eleito+1; i < T; i++) {
                rep ++;
                if(V[i] < menor) {
                    menor = V[i];
                    posMenor = i;
                }
            }
            if(V[posMenor] < V[eleito]) {
                int aux = V[posMenor];
                V[posMenor] = V[eleito];
                V[eleito] = aux;
                troc ++;
            }
        }
        System.out.println(rep + "\t" + troc);
    }
    
    public static void imprimeVetor(int[] V) {
        for(int i=0; i<V.length; i++) {
            System.out.print(V[i] + " ");
        }
        System.out.print("\n");
    }
    
    public static int[] inverteDados(int[] V) {
        int[] R = new int[V.length];
        for(int i=0; i < V.length; i++) {
            R[i] = V[V.length-i-1];
        }
        return R;
    }
    
    public static void geraVetores() {
        aleat1 = new int[TAMANHO];
        aleat2 = new int[TAMANHO];
        aleat3 = new int[TAMANHO];
        ordenado = new int[TAMANHO];
        
        Random rand = new Random();
        for(int i=0; i<aleat1.length; i++) {
            aleat1[i] = rand.nextInt(TAMANHO*2);
            aleat2[i] = aleat1[i];
            aleat3[i] = aleat1[i];
            ordenado[i] = rand.nextInt(TAMANHO*2);
        }
        
        Arrays.sort(ordenado);
        
        dec1 = Ordenacao.inverteDados(ordenado);
        dec2 = Ordenacao.inverteDados(ordenado);
        dec3 = Ordenacao.inverteDados(ordenado);
    }
}

