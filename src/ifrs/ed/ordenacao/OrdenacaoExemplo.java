package ifrs.ed.ordenacao;

import ifrs.utils.MedidorTempo;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;



public class OrdenacaoExemplo {
	public static int TAMANHO = 10000;
	public static int[] aleat1, aleat2, aleat3;
	public static int[] ordenado;
	public static int[] dec1, dec2, dec3;
	 
	public static void main(String[] args) {
				
		
		geraVetores();
		
		/*imprimeVetor(aleat1);
		
		imprimeVetor(ordenado);
		
		
		imprimeVetor(dec1);*/
		
		
		/* BUBBLE SORT */
		
		bubbleSort(aleat1);
		
		bubbleSort(ordenado);
		
		bubbleSort(dec1);
		
		bubbleSortOtim(aleat2);
		
		bubbleSortOtim(ordenado);
		
		bubbleSortOtim(dec2);
	
		
	}
		

	public static void bubbleSort(int[] V) {
		int T = V.length;
		int rep = 0, troc = 0;
		for(int n = T-1; n > 0; n--) {
			for(int i = 0; i < n; i++) {
				rep++;
				if(V[i] > V[i+1]) {
					int aux = V[i];
					V[i] = V[i+1];
					V[i+1] = aux;
					troc++;
				}
			}
		}
		System.out.println("REP: " + rep + " TROC: " + troc);
	}
	
	public static void bubbleSortOtim(int[] V) {
		int T = V.length;
		int rep = 0, troc = 0;
		for(int n = T-1; n > 0; n--) {
			boolean troca = false;
			for(int i = 0; i < n; i++) {
				rep++;
				if(V[i] > V[i+1]) {
					int aux = V[i];
					V[i] = V[i+1];
					V[i+1] = aux;
					troca = true;
					
					troc++;
				}
			}
			if(!troca) {
				break;
			}
		}
		System.out.println("REP: " + rep + " TROC: " + troc);
	}	
	
	public static void selectionSort(int[] V) {
		int T = V.length;
		for(int eleito = 0; eleito < T-1; eleito++) {
			/* busca do menor elemento à direita do eleito */
			int menor = Integer.MAX_VALUE;
			int posMenor = -1;
			for(int i = eleito+1; i < T; i++) {
				if(V[i] < menor) {
					menor = V[i];
					posMenor = i;
				}
			}
			if(V[posMenor] < V[eleito]) {
				int aux = V[posMenor];
				V[posMenor] = V[eleito];
				V[eleito] = aux;
			}
		}
	}
	
	public static void imprimeVetor(int[] V) {
		for(int i=0; i<V.length; i++) { 
			System.out.print(V[i] + " ");
		}
		System.out.print("\n");
	}
	
	public static int[] inverteDados(int[] V) {
		int[] R = new int[V.length];
		for(int i=0; i < V.length; i++) { 
			R[i] = V[V.length-i-1]; 
		}
		return R;
	}
	
	public static void geraVetores() {
		aleat1 = new int[TAMANHO];
		aleat2 = new int[TAMANHO];
		aleat3 = new int[TAMANHO];
		ordenado = new int[TAMANHO];
		
		Random rand = new Random();
		for(int i=0; i<aleat1.length; i++) {
			aleat1[i] = rand.nextInt(TAMANHO*2);
			aleat2[i] = rand.nextInt(TAMANHO*2);
			aleat3[i] = rand.nextInt(TAMANHO*2);
			ordenado[i] = rand.nextInt(TAMANHO*2);
		}
		
		Arrays.sort(ordenado);
		
		dec1 = Ordenacao.inverteDados(ordenado);
		dec2 = Ordenacao.inverteDados(ordenado);
		dec3 = Ordenacao.inverteDados(ordenado);
	}
}

