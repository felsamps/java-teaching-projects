package ifrs.ed.heap;

public class TesteHeap {
	
	public static void main(String[] args) {
		Heap h = new Heap(30);
		h.enfileira(1);
		h.enfileira(3);
		h.enfileira(2);
		h.enfileira(8);
		h.enfileira(4);
		h.enfileira(10);
		h.enfileira(0);
		h.enfileira(15);
		h.enfileira(11);
		h.enfileira(12);
		
		System.out.println(h);
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
		System.out.println(h.desenfileira());
	}
	
}