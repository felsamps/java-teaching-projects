package ifrs.ed.heap;


public class Heap {
	private int num;
	private int[] heap;
	
	public Heap(int max) {
		heap = new int[max];
		heap[0] = Integer.MAX_VALUE;
		num = 0;
	}
	
	private int pai(int i) {
		return i/2;
	}
	
	private int filhoEsquerda(int i) {
		return i*2;
	}
	
	private int filhoDireita(int i) {
		return i*2 + 1;
	}
	
	public void enfileira(int valor) {
		num ++;
		heap[num] = valor;
		atualizaPais(num);
	}
	
	private void atualizaPais(int i) {
		int valor = heap[i];
		
		while(valor > heap[pai(i)]) {
			heap[i] = heap[pai(i)];
			i /= 2;
		}
		
		heap[i] = valor;
	}
	
	public int desenfileira() {
		int valor = heap[1];
		heap[1] = heap[num+1];
		num --;
		atualizaFilhos(1);
		return valor;
	}
	
	private void atualizaFilhos(int i) {
		int valor = heap[i];
		while( i < num/2 ) {
			int filhoSerTrocado;
			if( heap[filhoEsquerda(i)] > heap[filhoDireita(i)]) {
				filhoSerTrocado = filhoEsquerda(i);
			}
			else {
				filhoSerTrocado = filhoDireita(i);
			}
			if(valor > heap[filhoSerTrocado]) {
				break;
			}
			heap[i] = heap[filhoSerTrocado];
			i = filhoSerTrocado;
		}
		heap[i] = valor;
	}
	
	public String toString() {
		String saida = "";
		for(int i = 1; i < num; i++) {
			saida += heap[i] + " ";
		}
		return saida;
	}
}