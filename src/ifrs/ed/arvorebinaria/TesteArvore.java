/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifrs.ed.arvorebinaria;

/**
 *
 * @author felsamps
 */
public class TesteArvore {
	public static void main(String[] args) {
		ArvoreBinaria ab = new ArvoreBinaria();
		
		ab.insere(30);
		ab.insere(15);
		ab.insere(10);
		ab.insere(7);
		ab.insere(12);
		ab.insere(18);
		ab.insere(40);
		ab.insere(39);
		ab.insere(50);
		ab.insere(45);
		
		System.out.println("Altura: " + ab.altura());
		
		ab.imprimeFormatoArvore();
	}
}
