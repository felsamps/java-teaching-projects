package ifrs.ed.arvorebinaria;

public class ArvoreBinaria {
	
	private class Nodo {
		public int item;
		public Nodo esq, dir;
	}
	
	private Nodo raiz;
	
	public ArvoreBinaria() {
		this.raiz = null;
	}
	
	public int altura() {
		return altura(this.raiz);
	}
	
	private int altura(Nodo n) {
		if(n == null) {
			return 0;
		}
		else {
			int altEsq = altura(n.esq);
			int altDir = altura(n.dir);
			return 1 + ( (altEsq > altDir) ? altEsq : altDir );
		}
	}
	
	public void insere(int item) {
		insere(item, this.raiz);
	}
	
	private void insere(int item, Nodo n) {
		if(n == this.raiz && n == null) {
			raiz = new Nodo();
			raiz.item = item;
			raiz.esq = null;
			raiz.dir = null;
		}
		else {
			if(item < n.item) {
				if(n.esq != null) {
					insere(item, n.esq);
				}
				else {
					n.esq = new Nodo();
					n.esq.item = item;
					n.esq.esq = null;
					n.esq.dir = null;
				}
			}			
			else {
				if(n.dir != null) {
					insere(item, n.dir);
				}
				else {
					n.dir = new Nodo();
					n.dir.item = item;
					n.dir.esq = null;
					n.dir.dir = null;
				}
			}
			
		}
	}
	
	public void imprimeFormatoArvore() {
		imprimeFormatoArvore(this.raiz, 0);
	}
	
	public void imprimeFormatoArvore(Nodo n, int nivel) {
		if(n == null) {
			return;
		}
		else {			
			imprimeFormatoArvore(n.dir, nivel+1);
			for (int i = 0; i < nivel; i++) {
				System.out.print("   ");				
			}			
			System.out.println(n.item);
			imprimeFormatoArvore(n.esq, nivel+1);
		}
	}
		
}
