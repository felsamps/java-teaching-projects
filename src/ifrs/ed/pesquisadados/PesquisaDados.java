
package ifrs.ed.pesquisadados;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import ifrs.utils.MedidorTempo;

public class PesquisaDados {

	private static int TAMANHO = 10000;
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		int[] vetor = new int[TAMANHO];
		Random r = new Random();
		MedidorTempo mt = new MedidorTempo();
		
		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = r.nextInt(TAMANHO);			
		}
				
		mt.inicia();

		Arrays.sort(vetor);
		
		for (int i = 0; i < 1000; i++) {
			boolean b = pesquisaSequencialOrd(vetor, r.nextInt(TAMANHO));
		}
		
		mt.termina();
		
		System.out.println(mt.getTimeInMilisec());
	}
	
	public static boolean pesquisaSequencial(int[] vetor, int valor) {
		for (int i = 0; i < vetor.length; i++) {
			if(valor == vetor[i]) {
				return true;
			}
		}
		return false;
	}
	public static boolean pesquisaSequencialOrd(int[] vetor, int valor) {
		for (int i = 0; i < vetor.length; i++) {
			if(valor == vetor[i]) {
				return true;
			}
			else {
				if(valor > vetor[i]) {
					return false;
				}
			}
		}
		return false;
	}
}
