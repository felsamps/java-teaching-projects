package ifrs.ed.estruturas;

import ifrs.ed.grafos.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Grafo {
		
	private LinkedList<Integer>[] listaAdj;
	private int numVertices;
	
	public Grafo(int numVertices) {
		this.listaAdj = new LinkedList[numVertices+1];
		this.numVertices = numVertices;
		
		for(int i=1; i<=numVertices; i++) {
			this.listaAdj[i] = new LinkedList();
		}
			
	}
	
	public void adicionaAresta(int v1, int v2) {
		this.listaAdj[v1].add(v2);
		this.listaAdj[v2].add(v1);
	}
	
	public void removeAresta(int v1, int v2) {
		this.listaAdj[v1].remove(v2);
		this.listaAdj[v2].remove(v1);
	}
	
	public void imprime() {
		String saida = "";
		for (int i = 1; i < listaAdj.length; i++) {
			saida += "Adjacências do vértice " + i + ": ";
			for(Integer adjacencia : listaAdj[i]) {
				saida += adjacencia + " ";
			}
			saida += "\n";
		}
		System.out.println(saida);
	}

	public int calculaDistanciaMinima(int verticeInicial, int verticeFinal) {
		Queue<Integer> fila = new LinkedList();
		boolean[] marcado = new boolean[this.numVertices+1];
		int[] distancia = new int[this.numVertices+1];
		
		for (int i = 0; i < marcado.length; i++) {
			marcado[i] = false;
			distancia[i] = 0;
		}
		
		fila.add(verticeInicial);
		distancia[verticeInicial] = 0;
		marcado[verticeInicial] = true;
		
		while(!fila.isEmpty()) {
			int vertice = fila.poll();
			
			for(Integer adjacencia : listaAdj[vertice]) {
				if(!marcado[adjacencia]) {
					fila.add(adjacencia);
					distancia[adjacencia] = distancia[vertice] + 1;
					marcado[adjacencia] = true;
				}
			}
		}
		return distancia[verticeFinal];
	}
}
