package ifrs.ed.estruturas;

public class TesteDistancias {
	public static void main(String[] args) {
		Grafo g = new Grafo(6);
		
		g.adicionaAresta(1, 2);
		g.adicionaAresta(2, 3);
		g.adicionaAresta(2, 4);
		g.adicionaAresta(3, 4);
		g.adicionaAresta(4, 5);
		g.adicionaAresta(5, 6);
		
		int acum = 0;
		acum += g.calculaDistanciaMinima(1, 3);
		acum += g.calculaDistanciaMinima(3, 6);
		acum += g.calculaDistanciaMinima(6, 2);
		
		System.out.println("Distância: " + acum);
		g.imprime();
	}
}
