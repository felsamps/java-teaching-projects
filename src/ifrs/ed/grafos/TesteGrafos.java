package ifrs.ed.grafos;

public class TesteGrafos {
	public static void main(String[] args) {
		
		// Exemplo 1
		/*Grafo g = new Grafo(6);
		g.adicionaAresta(1,3);
		g.adicionaAresta(2,3);
		g.adicionaAresta(2,5);
		g.adicionaAresta(3,4);
		g.adicionaAresta(3,5);
		g.adicionaAresta(5,6);
		g.adicionaAresta(4,6);
		*/
		
		// Exemplo 2
		
		Grafo g = new Grafo(5);
		g.adicionaAresta(5,4);
		g.adicionaAresta(5,1);
		g.adicionaAresta(4,1);
		g.adicionaAresta(4,3);
		g.adicionaAresta(3,1);
		g.adicionaAresta(1,2);
		g.adicionaAresta(3,2);
		
		
		
		System.out.println(g);
		
		g.percursoLargura(1);
		g.percursoProfundidade(1);
		
		
		/*
		GrafoValorado gv = new GrafoValorado(6);
		gv.adicionaAresta(1, 3, 2);
		gv.adicionaAresta(2, 3, 2);
		gv.adicionaAresta(2, 5, 5);
		gv.adicionaAresta(3, 4, 1);
		gv.adicionaAresta(3, 5, 3);
		gv.adicionaAresta(5, 6, 1);
		gv.adicionaAresta(4, 6, 1);
		
		System.out.println(gv);
		
		gv.menorCaminho(1, 6);
		
		*/
		
	}
}
