package ifrs.ed.grafos;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class GrafoValorado {

	
	private class Aresta {
		public int num;
		public int peso;
		public Aresta prox;
	}
	
	private Aresta[] listaAdj;
	private int numVertices;
	
	public GrafoValorado(int numVertices) {
		this.listaAdj = new Aresta[numVertices+1];
		this.numVertices = numVertices;
	}
	
	public void adicionaAresta(int v1, int v2, int peso) {
		Aresta a1 = new Aresta();
		a1.num = v2;
		a1.peso = peso;
		a1.prox = this.listaAdj[v1];
		this.listaAdj[v1] = a1;
		
		Aresta a2 = new Aresta();
		a2.num = v1;
		a2.peso = peso;
		a2.prox = this.listaAdj[v2];
		this.listaAdj[v2] = a2;		
	}
	
	public String toString() {
		String saida = "";
		for (int i = 1; i < listaAdj.length; i++) {
			saida += "Adjacências do vértice " + i + ": ";
			Aresta aux = listaAdj[i];
			while(aux != null) {
				saida += aux.num + " (" + aux.peso + ") ";
				aux = aux.prox;
			}
			saida += "\n";
		}
		return saida;
	}
	
	void menorCaminho(int verticeInicial, int verticeFinal) {
		Queue<Integer> fila = new LinkedList();
		boolean[] marcado = new boolean[this.numVertices+1];
		int[] distancia = new int[this.numVertices+1];

		for (int i = 0; i < marcado.length; i++) {
			marcado[i] = false;
			distancia[i] = Integer.MAX_VALUE;
		}
		
		fila.add(verticeFinal);
		marcado[verticeFinal] = true;
		distancia[verticeFinal] = 0;
		
		while(!fila.isEmpty()) {
			int t = fila.poll();
			marcado[t] = true;
			
			Aresta aux = this.listaAdj[t];
			while(aux != null) {
				int vertice = aux.num;
				int peso = aux.peso;
				
				if(distancia[vertice] > peso + distancia[t]) {
					distancia[vertice] = peso + distancia[t];
				}
				
				if(!marcado[vertice]) {
					fila.add(vertice);
					marcado[vertice] = true;
				}
				
				aux = aux.prox;
			}
		}
		
		System.out.println(Arrays.toString(distancia));
		
	}
}
