package ifrs.top.apresentacaotcp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ClienteUDP {
	public static void main(String[] args) {
		try {
				
			DatagramSocket socket = new DatagramSocket();
			byte[] dado;
			DatagramPacket datagrama;
			
			Scanner scan = new Scanner(System.in);
			String mensagemRecebida = "";
			String mensagemEnviada = "";
			
			String remoteIp = "192.168.6.210";
			
			while(true) {
				System.out.print(">> ");
				mensagemEnviada = scan.nextLine();
				dado = mensagemEnviada.getBytes();
				datagrama = new DatagramPacket(dado, dado.length, InetAddress.getByName(remoteIp), 8080);	
				socket.send(datagrama);
				
				
				dado = new byte[100];
				datagrama = new DatagramPacket(dado, dado.length);
				
				socket.receive(datagrama);
				mensagemRecebida = new String(datagrama.getData());
								
				System.out.println(remoteIp + ": " + mensagemRecebida);
			} 
			
		} catch (IOException ex) {
			System.out.println("Erro!");
		}
		
	}
}
