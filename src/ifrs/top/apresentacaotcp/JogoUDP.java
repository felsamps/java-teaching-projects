package ifrs.top.apresentacaotcp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author felsamps
 */
public class JogoUDP {
	public static void main(String[] args) {
		try {
				
			DatagramSocket socket = new DatagramSocket(8080);
			byte[] dado;
			DatagramPacket datagrama;
			
			Random gen = new Random();
			int alvo = gen.nextInt(10000);
			
			Integer numeroRecebido = -1;
			
			do {
				dado = new byte[100];
				datagrama = new DatagramPacket(dado, dado.length);
			
				socket.receive(datagrama);
				
				String mensagemRecebida = "";
				mensagemRecebida = new String(datagrama.getData());
				
				mensagemRecebida = mensagemRecebida.replaceAll("[^\\d.]", "");
				
				try {
					numeroRecebido = Integer.parseInt(mensagemRecebida);

					if(numeroRecebido < alvo) {
						System.out.println("O valor alvo é MAIOR que " + numeroRecebido + "!");
					}
					if(numeroRecebido > alvo) {
						System.out.println("O valor alvo é MENOR que " + numeroRecebido + "!");
					}
				}
				catch(NumberFormatException nfe) {
					System.out.println("\n");
				}
				
			} while(numeroRecebido != alvo);
			
			System.out.println("O cliente de IP " + datagrama.getAddress() + " acertou o alvo: " + alvo);
			
		} catch (IOException ex) {
			System.out.println("Erro durante o recebimento do pacote/envio do pacote!");
		}
		
	}
}
