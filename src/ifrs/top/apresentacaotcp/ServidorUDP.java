package ifrs.top.apresentacaotcp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.Scanner;

public class ServidorUDP {
	public static void main(String[] args) {
		try {
				
			DatagramSocket socket = new DatagramSocket(8080);
			byte[] dado;
			DatagramPacket datagrama;
			
			Scanner scan = new Scanner(System.in);
			String mensagemRecebida = "";
			String mensagemEnviada = "";
			
			String remoteIp = "";
			
			while(true) {
				dado = new byte[100];
				datagrama = new DatagramPacket(dado, dado.length);
				
				socket.receive(datagrama);
				mensagemRecebida = new String(datagrama.getData());
				remoteIp = datagrama.getAddress().getHostAddress();
				
				System.out.println(remoteIp + ": " + mensagemRecebida);
			
				System.out.print(">> ");
				mensagemEnviada = scan.nextLine();
				dado = mensagemEnviada.getBytes();
				datagrama = new DatagramPacket(dado, dado.length, InetAddress.getByName(remoteIp), 8080);	
				socket.send(datagrama);
				
			} 
			
		} catch (IOException ex) {
			System.out.println("Erro!");
		}
		
	}
}
