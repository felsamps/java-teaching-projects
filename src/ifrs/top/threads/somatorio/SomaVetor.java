package ifrs.top.threads.somatorio;

public class SomaVetor extends Thread {
	private int i, f;
	private long resultado;
	
	public SomaVetor(int i, int f) {
		this.i = i;
		this.f = f;
		this.resultado = 0;
	}
	
	@Override
	public void run() {
		for(int cont = i; cont < f; cont++) {
			this.resultado += cont;
		}
		
	}
	
	public long getResultado() {
		return this.resultado;
	}
	
}
