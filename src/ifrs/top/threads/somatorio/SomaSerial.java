package ifrs.top.threads.somatorio;

import ifrs.utils.MedidorTempo;
import java.util.ArrayList;

public class SomaSerial {
	public static int T = 1000000000;
	public static void main(String[] args) {
		long resultadoFinal = 0;
		
		MedidorTempo t1 = new MedidorTempo();
		
		t1.inicia();
		
		for(int cont = 0; cont < T; cont++) {
			resultadoFinal += cont;
		}
		t1.termina();
		
		System.out.println("Resultado = " + resultadoFinal);
		System.out.println("Tempo Serial = " + t1.getTimeInMilisec());
	}
}
