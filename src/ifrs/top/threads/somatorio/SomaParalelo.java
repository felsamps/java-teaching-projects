package ifrs.top.threads.somatorio;

import ifrs.utils.MedidorTempo;

public class SomaParalelo {
	public static int T = 1000000000;
	public static void main(String[] args)  {
		
		MedidorTempo t1 = new MedidorTempo();
						
		SomaVetor s1 = new SomaVetor(0, T/2);
		SomaVetor s2 = new SomaVetor(T/2, T);
		
		t1.inicia();
		s1.start();
		s2.start();
		
		try {
			s1.join();
			s2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t1.termina();
		
		long resultadoFinal = s1.getResultado() + s2.getResultado();
		System.out.println("Resultado = " + resultadoFinal);
		System.out.println("Tempo Paralelo = " + t1.getTimeInMilisec());
	}
}
