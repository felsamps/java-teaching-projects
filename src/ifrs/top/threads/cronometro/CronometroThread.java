package ifrs.top.threads.cronometro;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CronometroThread extends Thread {
    
    private int hora, min, seg;
    private CronometroView view;
    private boolean ativo, marca;
    
    public CronometroThread() {
        this.hora = 0;
        this.min = 0;
        this.seg = 0;
        this.view = new CronometroView();
        this.ativo = true;
    }
    
    @Override
    public void run() {
        this.view.setVisible(true);
        
        while(true) {
            if(this.ativo) {
                this.seg ++;            
                if(this.seg == 60) {
                    this.seg = 0;
                    this.min ++;
                    if(this.min == 60) {
                        this.min = 0;
                        this.hora ++;
                    }
                }

                this.view.atualizaCampos(this.seg, this.min, this.hora);
                if(this.marca) {
                    this.view.registraTempo();
                    this.marca = false;
                }

            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                Logger.getLogger(CronometroThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    void registraTempo() {
        this.marca = true;
    }
    
}
