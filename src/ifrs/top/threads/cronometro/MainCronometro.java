package ifrs.top.threads.cronometro;

import java.util.Scanner;

public class MainCronometro {

    public static void main(String[] args) {
        CronometroThread crono0 = new CronometroThread();
        crono0.start();
        
        Scanner scan = new Scanner(System.in);
        
        String opcao = "*";        
        while(!opcao.equals("sair")) {
            opcao = scan.next();
            
            if(opcao.equals("para")) {
                crono0.setAtivo(false);
            }
            else if(opcao.equals("cont")) {
                crono0.setAtivo(true);
            }
            else if(opcao.equals("marca")) {
                crono0.registraTempo();
            }
        }
        
    }
    
}
