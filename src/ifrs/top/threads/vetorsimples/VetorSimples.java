package ifrs.top.threads.vetorsimples;

import java.util.Arrays;
import java.util.Random;

public class VetorSimples {
	private int[] vetor;
	private int cont;
	
	public VetorSimples(int max) {
		vetor = new int[max];
		cont = 0;
	}
	
	public synchronized void adiciona(int valor) {
		
		vetor[cont] = valor;
		System.out.println("Thread: " + Thread.currentThread().getId() + " Escrito o valor " + valor + " na posição " + cont);
		cont++;
	}
	
	public String toString() {
		return Arrays.toString(vetor);
	}
}
