package ifrs.top.threads.vetorsimples;

public class MainVetor {
	
	public static void main(String[] args) {
		VetorSimples vs = new VetorSimples(6);
		
		PreenchedorVetor pv1 = new PreenchedorVetor(vs, 1);
		PreenchedorVetor pv2 = new PreenchedorVetor(vs, 10);
		
		pv1.start();
		pv2.start();
		
		try {
			pv1.join();
			pv2.join();
		}
		catch(InterruptedException e) {
			System.out.println("Erro no join!");
		}
		
		System.out.println(vs);
		
	}
	
}
