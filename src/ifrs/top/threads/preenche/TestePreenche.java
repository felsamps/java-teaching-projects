package ifrs.top.threads.preenche;


public class TestePreenche {
    public static void main(String[] args) {
        Vetor v = new Vetor();
        
        PreencheVetor pvA = new PreencheVetor(v, 'A');
        PreencheVetor pvB = new PreencheVetor(v, 'B');
        
        PreencheVetor pvC = new PreencheVetor(v, 'C');
        
        pvA.start();
        pvB.start();
        pvC.start();
        
        try {
            pvA.join();
            pvB.join();
            pvC.join();
        }
        catch(InterruptedException e) {
            System.out.println("Erro!");
        }
        
        v.imprime();
    }
}
