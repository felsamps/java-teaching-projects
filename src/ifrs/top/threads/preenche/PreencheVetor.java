package ifrs.top.threads.preenche;

import java.util.Random;

public class PreencheVetor extends Thread {
    Vetor vetor;
    private char c;
    
    public PreencheVetor(Vetor vetor, char c) {
        this.vetor = vetor;
        this.c = c;
    }
    
    public void run() {

        for(int i=0; i < 20; i++) {
            this.vetor.preenche(c, i);
        }
        
    }
    
}
