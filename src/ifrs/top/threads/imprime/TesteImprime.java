package ifrs.top.threads.imprime;

public class TesteImprime {
    public static void main(String[] args) {
        ImprimeValores i1 = new ImprimeValores();
        ImprimeValores i2 = new ImprimeValores();
        ImprimeValores i3 = new ImprimeValores();

        i1.start();
        i2.start();
        i3.start();
    }
}
