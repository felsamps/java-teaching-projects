package ifrs.top.threads.produtorconsumidor;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Deposito {
	private int itens;
	private int capacidade;
	
	private boolean vazio, cheio;
	
	public Deposito(int capacidade, int itens) {
		this.capacidade = capacidade;
		this.itens = itens;
		
		System.out.println("Capacidade: " + capacidade + " Estoque: " + itens);
		
		atualiza();
	}
	
	public void atualiza() {
		if(itens == 0) {
			this.vazio = true;
		}
		else {
			this.vazio = false;
		}
		if(itens == capacidade) {
			this.cheio = true;
		}
		else {
			this.cheio = false;
		}
	}
	
	public synchronized void retira(int n) {
		
		while(vazio) {
			try {
				System.out.println("Consumidor esperando...");
				wait();
			}
			catch(InterruptedException e) {
				System.out.println("Erro!");
			}
		}
		
		if(!vazio) {
			itens -= n;
			atualiza();
			System.out.println("Item retirado! Estoque: " + itens);
			notifyAll();
		}
		
	}
	
	public synchronized void coloca(int n) {
		while(cheio) {
			try {
				System.out.println("Produtor esperando...");
				wait();
			}
			catch(InterruptedException e) {
				System.out.println("Erro!");
			}
		}
		
		if(!cheio) {
			itens +=n;
			atualiza();
			System.out.println("Item depositado! Estoque: " + itens);
			notifyAll();
			
		}
		
		
	}
}
