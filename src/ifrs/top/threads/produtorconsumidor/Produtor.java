package ifrs.top.threads.produtorconsumidor;

import java.util.Random;


public class Produtor extends Thread {
	private Deposito dep;
	
	public Produtor(Deposito dep) {
		this.dep = dep;
	}
	
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(new Random().nextInt(3000));
				dep.coloca(1);
			}
			catch(InterruptedException e) {
				System.out.println("Erro durante a execução da Thread!");
			}
		}
	}
}
