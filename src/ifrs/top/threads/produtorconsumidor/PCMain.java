package ifrs.top.threads.produtorconsumidor;


public class PCMain {
	
	public static void main(String[] args) {
		Deposito dep = new Deposito(5, 5);

		Produtor prod = new Produtor(dep);
		Consumidor cons = new Consumidor(dep);
		
		prod.start();
		cons.start();

	}
	
	
}
