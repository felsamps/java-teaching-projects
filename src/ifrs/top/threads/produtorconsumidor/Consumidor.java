package ifrs.top.threads.produtorconsumidor;

import java.util.Random;

public class Consumidor extends Thread {
	private Deposito dep;
	
	public Consumidor(Deposito dep) {
		this.dep = dep;
	}
	
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(new Random().nextInt(3000));
				dep.retira(1);
			}
			catch(InterruptedException e) {
				System.out.println("Erro durante a execução da Thread!");
			}
		}
	}
}
