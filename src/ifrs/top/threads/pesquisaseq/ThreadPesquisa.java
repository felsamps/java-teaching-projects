
package ifrs.top.threads.pesquisaseq;

public class ThreadPesquisa extends Thread {
    private int[] vetor;
    private int i, f;
    private int valor;
    private boolean achou;
    private long id;
    
    public ThreadPesquisa(int[] v, int i, int f, int valor) {
        this.vetor = v;
        this.i = i;
        this.f = f;
        this.valor = valor;
        this.achou = false;
        this.id = getId();
    }
    
    @Override
    public void run() {
        for (int j = i; j < f; j++) {
            
            System.out.println("ID: " + this.id + " testando o valor " + vetor[j]);
            
            if(vetor[j] == valor) {
                this.achou = true;
                System.out.println("Achou! ID: " + this.id);                
                return;
            }            
        }
    }
    
    public boolean achouValor() {
        return this.achou;
    }
}
