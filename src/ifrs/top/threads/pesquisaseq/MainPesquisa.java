package ifrs.top.threads.pesquisaseq;

import java.util.Arrays;
import java.util.Random;


public class MainPesquisa {
    private static int TAM = 20;
    
    public static void main(String[] args) {
        int[] vetor = geraVetor();
        
        ThreadPesquisa t0 = new ThreadPesquisa(vetor, 0, TAM/2, 5);
        ThreadPesquisa t1 = new ThreadPesquisa(vetor, TAM/2, TAM, 5);
        
        System.out.println(Arrays.toString(vetor));
        
        t0.start();
        t1.start();
        
        //while(t0.isAlive() || t1.isAlive()) {
            //System.out.println("AA");
        //}
        
        
        if(t0.achouValor() || t1.achouValor()) {
            System.out.println("Valor encontrado!");
        }
        else {
            System.out.println("Valor não encontrado!");
        }
    }

    private static int[] geraVetor() {
        int[] returnable = new int[TAM];
        Random r = new Random();
        for (int i = 0; i < TAM; i++) {
            returnable[i] = r.nextInt(TAM/2);
        }
        return returnable;
    }
}
