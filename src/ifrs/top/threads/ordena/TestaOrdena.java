package ifrs.top.threads.ordena;

import ifrs.utils.MedidorTempo;
import java.util.Arrays;
import java.util.Random;


public class TestaOrdena {
	public static int TAMANHO = 100000;
	
	public static void main(String[] args) {
		int[] vet = new int[TAMANHO];
		Random r = new Random();
		
		for (int i = 0; i < TAMANHO; i++) {
			vet[i] = r.nextInt(TAMANHO*2);
		}
		
		MedidorTempo mt = new MedidorTempo();
		mt.inicia();
		vet = ordena(vet);
		mt.termina();
		
		System.out.println("Tempo: " + mt.getTimeInMilisec());
		
		for (int i = 0; i < vet.length; i++) {
			System.out.print(vet[i] + " ");
			
		}
		
		
	}
	
	public static int[] ordena(int[] V) {
		Arrays.sort(V);
		return V;
	}
	
	public static int[] ordenaParalelo(int[] V) {
		ThreadOrdena t0 = new ThreadOrdena(V, 0, TAMANHO/2);
		ThreadOrdena t1 = new ThreadOrdena(V, TAMANHO/2, TAMANHO);
		
		int[] VT = new int[TAMANHO];
		
		t0.start();
		t1.start();
		
		try {
			t0.join();
			t1.join();
		}
		catch(InterruptedException exc) {
			System.out.println("Erro!");
		}
		
		int i0 = 0;
		int i1 = TAMANHO/2;
		
		for(int k=0; k<TAMANHO; k++) {
			if(i0 >= TAMANHO/2) {
				VT[k] = V[i1];
				i1++;
			}
			else {
				if(i1 >= TAMANHO) {
					VT[k] = V[i0];
					i0++;
				}
				else {
			
					if(V[i0] < V[i1]) {
						VT[k] = V[i0];
						i0++;
						
					}
					else {
						VT[k] = V[i1];
						i1++;
					}
				}
			}
		}
		return VT;		
	}
}

