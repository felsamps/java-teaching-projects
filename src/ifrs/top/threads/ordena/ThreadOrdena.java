package ifrs.top.threads.ordena;

import java.util.Arrays;


public class ThreadOrdena extends Thread {
	private int[] V;
	private int inicio, fim;
	
	public ThreadOrdena(int[] V, int inicio, int fim) {
		this.V = V;
		this.inicio = inicio;
		this.fim = fim;
	}
	
	@Override
	public void run() {
		Arrays.sort(V, inicio, fim);
	}
}
