package ifrs.top.threads.maior;

import ifrs.utils.MedidorTempo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main4Threads {
    
    private static int N = 100000000;
    
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<Integer>();
        for (int i = 0; i < N; i++){
            lista.add(i);
        }
        Collections.shuffle(lista);
        Integer[] array = new Integer[N];
        array = lista.toArray(array);
        
        MedidorTempo mt = new MedidorTempo();
        
        MaiorThread t0 = new MaiorThread(array, 0, N/4);
        MaiorThread t1 = new MaiorThread(array, N/4, N/2);
        MaiorThread t2 = new MaiorThread(array, N/2, 3*(N/4));
        MaiorThread t3 = new MaiorThread(array, 3*(N/4), N);
        
        mt.inicia();
        
        t0.start();
        t1.start();
        t2.start();
        t3.start();
        
        try {
            t0.join();
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main2Threads.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int[] maiores = new int[4];
        maiores[0] = t0.getMaior();
        maiores[1] = t1.getMaior();
        maiores[2] = t2.getMaior();
        maiores[3] = t3.getMaior();
        
        Arrays.sort(maiores);
        int maior = maiores[3];
        
        mt.termina();
        
        System.out.println("Maior elemento: " + maior);
        System.out.println("Tempo: " + mt.getTimeInMilisec() );
        
    }
}
