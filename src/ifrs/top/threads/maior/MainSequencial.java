
package ifrs.top.threads.maior;

import ifrs.utils.MedidorTempo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainSequencial {
       
    private static int N = 100000000;

    
    public static void main(String[] args) {
        
        Random rand = new Random();
        
        Integer[] array = new Integer[N];
        for (int i = 0; i < N; i++){
            array[i] = rand.nextInt(N*2);
        }

        MedidorTempo mt = new MedidorTempo();
        mt.inicia();
        
        int maior = -1;
        for (int j = 0; j < array.length; j++) {
            if(array[j] > maior) {
                maior = array[j];
            }
        }
        
        mt.termina();
        
        System.out.println("Maior elemento: " + maior);
        System.out.println("Tempo: " + mt.getTimeInMilisec() );
    
    }
}
