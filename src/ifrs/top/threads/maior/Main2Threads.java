package ifrs.top.threads.maior;

import ifrs.utils.MedidorTempo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main2Threads {
    
    private static int N = 100000000;
    
    public static void main(String[] args) {
        Random rand = new Random();
        
        Integer[] array = new Integer[N];
        for (int i = 0; i < N; i++){
            array[i] = rand.nextInt(N*2);
        }
        
        MedidorTempo mt = new MedidorTempo();
        
        MaiorThread t0 = new MaiorThread(array, 0, N/2);
        MaiorThread t1 = new MaiorThread(array, N/2, N);
        
        mt.inicia();
        
        t0.start();
        t1.start();
        
        try {
            t0.join();
            t1.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main2Threads.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        int[] maiores = new int[2];
        maiores[0] = t0.getMaior();
        maiores[1] = t1.getMaior();
        
        Arrays.sort(maiores);
        int maior = maiores[1];
        
        mt.termina();
        
        System.out.println("Maior elemento: " + maior);
        System.out.println("Tempo: " + mt.getTimeInMilisec() );
        
    }
}
