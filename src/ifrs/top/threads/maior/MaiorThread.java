package ifrs.top.threads.maior;

public class MaiorThread extends Thread {
    private Integer[] vetor;
    private int i, f;
    private int maior;
    
    public MaiorThread(Integer[] vetor, int i, int f) {
        this.i = i;
        this.f = f;
        this.vetor = vetor;
        this.maior = -1;
    }
    
    public void run() {
        for (int j = i; j < f; j++) {
            if(this.vetor[j] > this.maior) {
                this.maior = this.vetor[j];
            }
        }
    }
    
    public int getMaior() {
        return this.maior;
    }
}
