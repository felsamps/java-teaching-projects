package ifrs.top.tcpudp.chat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import javax.swing.JOptionPane;

public class ComunicacaoUDP {
	private String enderecoIp;
	private int porta;
	
	public ComunicacaoUDP(String enderecoIp, int porta) {
		this.enderecoIp = enderecoIp;
		this.porta = porta;	
	}
	
	public void enviaMensagem(String msg) {
		try {
			DatagramSocket socket = new DatagramSocket();
			byte[] dado = (msg + "\n").getBytes();
			DatagramPacket datagrama = new DatagramPacket(dado, dado.length, InetAddress.getByName(this.enderecoIp), 8080);
			
			socket.send(datagrama);
			
			socket.close();
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "Erro durante envio de mensagens via UDP!");
		}
	}
	
	public String recebeMensagem() {
		String msg = "";
		try {
			DatagramSocket socket = new DatagramSocket(porta);
			byte[] dado = new byte[100];
			DatagramPacket datagrama = new DatagramPacket(dado, dado.length);
			
			socket.receive(datagrama);
			
			msg = new String(datagrama.getData());
			
			socket.close();
						
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "Erro durante recebimento de mensagens via UDP!");
		}
		return msg;
	}

	public String getEnderecoIP() {
		return this.enderecoIp;
	}
	
	public static String acessaIPLocal(String niStr) {
		Enumeration nis;
		try {
			nis = NetworkInterface.getNetworkInterfaces();
			while(nis.hasMoreElements()) {
				NetworkInterface ni = (NetworkInterface) nis.nextElement();  
				Enumeration ias = ni.getInetAddresses();
				while (ias.hasMoreElements()) {  
					InetAddress ia = (InetAddress) ias.nextElement();
					if (!ni.getName().equals("lo")) {
						return ia.getHostAddress();   
					}
				}  
			}
		} catch (SocketException ex) {
			JOptionPane.showConfirmDialog(null, "Erro ao iniciar dispositivos de redes!");
		}
		return "localhost";
		
	}
}
