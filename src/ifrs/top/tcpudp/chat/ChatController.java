package ifrs.top.tcpudp.chat;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ChatController {
	private ChatView view;
	private boolean isServidor;
	private ComunicacaoUDP comm;
	private ThreadRecebeMensagens thread;
	
	public ChatController(ChatView view) {
		this.view = view;		
		this.view.iniciaIPLocal("192.168.6.204");
		this.view.mostraExemploRemoto();
	}
	
	

	public void iniciaChat(int porta, String enderecoIp) {
		this.comm = new ComunicacaoUDP(enderecoIp, porta);
		this.thread = new ThreadRecebeMensagens(comm, this);
		this.thread.start();
	}

	public void atualizaChat(String msg) {
		this.view.atualizaChat(this.comm.getEnderecoIP() + ": " + msg);
	}

	public void enviaMensagem(String msg) {
		this.comm.enviaMensagem(msg);
		this.view.atualizaChat("Eu: " + msg);
	}
	
	
	
}
