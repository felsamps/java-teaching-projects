package ifrs.top.tcpudp.chat;

public class ThreadRecebeMensagens extends Thread {
	private ComunicacaoUDP comm;
	private ChatController controller;
	
	public ThreadRecebeMensagens(ComunicacaoUDP comm, ChatController controller) {
		this.comm = comm;
		this.controller = controller;
	}
	
	public void run() {
		while(true) {
			String msg = this.comm.recebeMensagem();
			this.controller.atualizaChat(msg);
		}
	}	
}
