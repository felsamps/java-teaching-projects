package ifrs.top.tcpudp.jogovelha;

public class JogoVelhaController {	
	
	private JogoVelhaView view;
	private JogoVelha jogo;
	private boolean modo;
	
	public JogoVelhaController(JogoVelhaView view) {
		this.view = view;
		this.jogo = new JogoVelha();
	}
	
	public void setServidor() {
		this.modo = true;
	}
	
	public void setCliente() {
		this.modo = false;
	}
	
	public boolean isServidor() {
		return this.modo;
	}
	
	public void joga(int idJogada) {
		this.view.setaJogada(true, idJogada);
	}
	
	
	
	
	
	
}
