package ifrs.top.produtorconsumidorparque;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Parque {
	private String[] nomeBrinquedos;
	private Crianca[] brinquedos;
	
	public Parque() {
		brinquedos = new Crianca[4];
		nomeBrinquedos = new String[4];
		
		nomeBrinquedos[0] = "Gangorra";
		nomeBrinquedos[1] = "Escorregador";
		nomeBrinquedos[2] = "Balanço";
		nomeBrinquedos[3] = "Carrossel";
		
	}
	
	public synchronized int brinca(Crianca c, int idBrinquedo) {
		
		while(brinquedos[idBrinquedo] != null) {
			try {
				System.out.println(c.getNome() + " a espera do " + nomeBrinquedos[idBrinquedo]);
				wait();
			} catch (InterruptedException ex) {
				System.out.println("Erro!");
			}
		}
		
		brinquedos[idBrinquedo] = c;
		System.out.println(c.getNome() + " brinca com " + nomeBrinquedos[idBrinquedo]);
		
		return idBrinquedo;
	
		
	}

	public synchronized void deixaBrincar(int id) {
		System.out.println(brinquedos[id].getNome() + " deixa de brincar com " + nomeBrinquedos[id]);
		brinquedos[id] = null;				
		notifyAll();
	}
	
	
	
	
}
