package ifrs.top.produtorconsumidorparque;

public class TesteParque {
	
	
	public static void main(String[] args) {
		Parque p = new Parque();
		Crianca c0 = new Crianca("Felipe", p);
		Crianca c1 = new Crianca("Bruno", p);
		Crianca c2 = new Crianca("Paulo", p);
		Crianca c3 = new Crianca("Rodrigo", p);
		
		c0.start();
		c1.start();
		c2.start();
		c3.start();
	}
	
}
