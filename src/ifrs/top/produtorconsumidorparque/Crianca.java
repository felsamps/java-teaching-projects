package ifrs.top.produtorconsumidorparque;

import java.util.Random;

public class Crianca extends Thread {
	private String nome;
	private Parque parque;
	
	public Crianca(String n, Parque p) {
		nome = n;
		parque = p;
	}
	
	public void run() {
		Random r = new Random();
		for (int i = 0; i < 2; i++) {
			try {
				Thread.sleep(r.nextInt(3000));
				int id = parque.brinca(this, r.nextInt(4));
				Thread.sleep(1000);
				parque.deixaBrincar(id);
			} catch (InterruptedException ex) {
				System.out.println("Erro!");
			}
		}
	}

	public String getNome() {
		return nome;
	}
}
