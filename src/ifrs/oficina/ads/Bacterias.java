/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifrs.oficina.ads;

import java.util.Scanner;

/**
 *
 * @author felsamps
 */
public class Bacterias {
	
	public static double logOfBase(int base, int num) {
		return Math.log(num) / Math.log(base);
	}
	
	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int num;
        int divMaior=0, diasMaior=0, iMaior=0;
        int div, dias;
        
        num = scan.nextInt();
        
        for(int i=0; i<num; i++) {
            div = scan.nextInt();
            dias = scan.nextInt();
            
            if(i == 0) {
                diasMaior = dias;
                divMaior = div;
                iMaior = i;
            }
            else {
                double F = logOfBase(divMaior, div) * (dias - 1);
				double FM = diasMaior-1;

				if(F > FM) {
					diasMaior = dias;
					divMaior = div;
					iMaior = i;
				}
			}            
            
        }
		
		System.out.println(iMaior);
		
    }

}
