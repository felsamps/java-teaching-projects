/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifrs.tap.ativpesquisa;

/**
 *
 * @author fmsampaio
 */
public class Contato implements Comparable<Contato> {
    private String nome, telefone, email;
    
    public Contato(String n, String t, String e) {
        this.nome = n;
        this.telefone = t;
        this.email = e;
    }
    
    @Override
    public String toString() {
        return this.getNome() + "-" + this.getTelefone() + "-" + this.getEmail() +"\n";
    }
    
    @Override
    public int compareTo(Contato o) {
        return this.getNome().compareTo(o.getNome());
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
