package ifrs.tap.ativpesquisa;

import ifrs.utils.MedidorTempo;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal {
    public static void main(String[] args) {
        //Contato[] vetor = importaArquivo("/home/fmsampaio/MEGA/IFRS/Ensino/Disciplinas/Int_TópicosAvançadosProgramação/2017/Códigos/Atividade1_ListaContatos/contatos1.txt");
        //Contato[] vetor = importaArquivo("/home/fmsampaio/MEGA/IFRS/Ensino/Disciplinas/Int_TópicosAvançadosProgramação/2017/Códigos/Atividade1_ListaContatos/contatos2.txt");
        //Contato[] vetor = importaArquivo("/home/fmsampaio/MEGA/IFRS/Ensino/Disciplinas/Int_TópicosAvançadosProgramação/2017/Códigos/Atividade1_ListaContatos/contatos3.txt");
        Contato[] vetor = importaArquivo("/home/fmsampaio/MEGA/IFRS/Ensino/Disciplinas/Int_TópicosAvançadosProgramação/2017/Códigos/Atividade1_ListaContatos/contatos4.txt");
        
        Arrays.sort(vetor);

        
        System.out.println(Arrays.toString(vetor));
        
        MedidorTempo mt1 = new MedidorTempo();
        MedidorTempo mt2 = new MedidorTempo();
        
        mt2.inicia();
        pesquisaSequencial(vetor, "Aaron");
        mt2.termina();
        
        
        mt1.inicia();
        pesquisaBinaria(vetor, "Aaron");
        mt1.termina();
        
        System.out.println("Tempo Busca Sequencial: " + mt2.getTimeInMilisec());
        System.out.println("Tempo Busca Binária: " + mt1.getTimeInMilisec());
        
    }

    private static Contato[] importaArquivo(String contatos) {
        try {
            FileReader fr = new FileReader(contatos);
            Scanner scan = new Scanner(fr);
            
            int numContatos = scan.nextInt();
            Contato[] cadastro = new Contato[numContatos];
            int i=0;
            
            scan.nextLine();
            
            while(scan.hasNext()) {
                String line = scan.nextLine();
                String[] tokens = line.split(";");
                
                cadastro[i++] = new Contato(tokens[0], tokens[1], tokens[2]);
            }
            
            return cadastro;
        } catch (FileNotFoundException ex) {
            System.out.println("Erro na abertura do arquivo!");
        }
        
        return null;
        
    }

    private static Contato pesquisaSequencial(Contato[] vetor, String nome) {
        for (int i = 0; i < vetor.length; i++) {
            if(nome.equals(vetor[i].getNome())) {
                return vetor[i];
            }
        }
        return null;
    }
    
       private static Contato pesquisaBinaria(Contato[] vetor, String nome) {
           int esq = 0;
           int dir = vetor.length -1;
           int meio;
           
           while(esq <= dir) {
               meio = (dir + esq) / 2;
               if(nome.compareTo(vetor[meio].getNome()) == 0) {
                   return vetor[meio];
               }
               else if(nome.compareTo(vetor[meio].getNome()) < 0) {
                   dir = meio - 1;
               }
               else {
                   esq = meio + 1;
               }
           }
           
           return null;
       }

}
