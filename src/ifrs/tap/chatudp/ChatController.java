package ifrs.tap.chatudp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class ChatController {
    private ComunicacaoUDP recebe;
    private ArrayList<ComunicacaoUDP> listaEnvios;
    private ThreadRecebeMensagens threadRecebe;
    
    public ChatController(String arquivoListaDispositivos, int portaEntrada) throws FileNotFoundException {
        this.listaEnvios = new ArrayList<>();
        
        Scanner scan = new Scanner(new File(arquivoListaDispositivos));
        while(scan.hasNext()) {
            String[] linha = scan.nextLine().split(" ");
            this.listaEnvios.add(new ComunicacaoUDP(linha[0], Integer.parseInt(linha[1]), 0000));
        }
        this.recebe = new ComunicacaoUDP("", 0000, portaEntrada);
        
        this.threadRecebe = new ThreadRecebeMensagens(this.recebe, this);
        
        
    }
    
    public synchronized void atualizaChat(Mensagem msg) {
        System.out.println(msg + "\n >>");
    }
    
    public void iniciaChat() {
        Scanner scan = new Scanner(System.in);
        
        this.threadRecebe.start();
        
        while(true) {
            System.out.print(">> ");
            String msg = scan.nextLine();
            this.enviaMensagemParaDispositivos(msg);
        }
    }

    private void enviaMensagemParaDispositivos(String msg) {
        for (ComunicacaoUDP envio : this.listaEnvios) {
            envio.enviaMensagem(msg);
        }
    }    

}
