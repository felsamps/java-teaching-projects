package ifrs.tap.chatudp;

import java.io.FileNotFoundException;

public class ChatPrincipal {
    public static void main(String[] args) {
        ChatController controller;
        
        try {
            controller = new ChatController("/home/fmsampaio/Desktop/lista_ips.txt", 12345);
            controller.iniciaChat();
        }
        catch(FileNotFoundException fnfe) {
            System.out.println("Erro na abertura do arquivo...");
        }
                
    }
}
