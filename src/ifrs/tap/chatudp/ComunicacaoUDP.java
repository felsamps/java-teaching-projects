package ifrs.tap.chatudp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import javax.swing.JOptionPane;

public class ComunicacaoUDP {
    private String enderecoIp;
    private int portaSaida;
    private int portaEntrada;
    
    public ComunicacaoUDP(String enderecoIp, int portaSaida, int portaEntrada) {
        this.enderecoIp = enderecoIp;
        this.portaSaida = portaSaida;
        this.portaEntrada = portaEntrada;
    }
    
    public void enviaMensagem(String msg) {
        try {
            DatagramSocket socket = new DatagramSocket();
            byte[] dado = (msg + "\n").getBytes();
            DatagramPacket datagrama = new DatagramPacket(dado, dado.length, InetAddress.getByName(this.enderecoIp), portaSaida);
            
            socket.send(datagrama);
            
            socket.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro durante envio de mensagens via UDP!");
        }
    }
    
    public Mensagem recebeMensagem() {
        String msg = "";
        InetAddress ip = null;
        try {
            DatagramSocket socket = new DatagramSocket(portaEntrada);
            byte[] dado = new byte[100];
            DatagramPacket datagrama = new DatagramPacket(dado, dado.length);
            socket.receive(datagrama);
            
            msg = new String(datagrama.getData());
            ip = datagrama.getAddress();
                        
            socket.close();
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Erro durante recebimento de mensagens via UDP!");
        }
        return new Mensagem(ip, msg);
    }
    
    public String getEnderecoIP() {
        return this.enderecoIp;
    }
    
    public static String acessaIPLocal(String niStr) {
        Enumeration nis;
        try {
            nis = NetworkInterface.getNetworkInterfaces();
            while(nis.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) nis.nextElement();
                Enumeration ias = ni.getInetAddresses();
                while (ias.hasMoreElements()) {
                    InetAddress ia = (InetAddress) ias.nextElement();
                    if (!ni.getName().equals("lo")) {
                        return ia.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            JOptionPane.showConfirmDialog(null, "Erro ao iniciar dispositivos de redes!");
        }
        return "localhost";
        
    }
}
