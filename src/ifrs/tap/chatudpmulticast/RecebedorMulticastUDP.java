package ifrs.tap.chatudpmulticast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class RecebedorMulticastUDP extends Thread {
    private MulticastSocket socket = null;
    private byte[] buff = new byte[256];
    private String enderecoMulticast;
    private int porta;
    
    public RecebedorMulticastUDP(String enderecoMulticast, int porta) {
        this.enderecoMulticast = enderecoMulticast;
        this.porta = porta;
    }
    
    public void run()  {
        try{
            this.socket = new MulticastSocket(this.porta);
            InetAddress grupo = InetAddress.getByName(this.enderecoMulticast);
            this.socket.joinGroup(grupo);
            while(true) {
                DatagramPacket datagrama = new DatagramPacket(buff, buff.length);
                this.socket.receive(datagrama);
                String msg = new String(datagrama.getData(), 0, datagrama.getLength());
                InetAddress ip = datagrama.getAddress();
                System.out.println("\n" + new Mensagem(ip, msg));
                System.out.print(">> ");
            }
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
            
    }
            
}
