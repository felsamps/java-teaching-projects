package ifrs.tap.chatudpmulticast;

import ifrs.tap.chatudp.*;
import java.net.InetAddress;


public class Mensagem {
    private InetAddress ip;
    private String msg;
    
    public Mensagem(InetAddress ip, String msg) {
        this.ip = ip;
        this.msg = msg;
    }
    
    public String toString() {
        return this.ip + ": " + this.msg + "\n";
    }
}
