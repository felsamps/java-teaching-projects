package ifrs.tap.chatudpmulticast;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ChatController {
    private RecebedorMulticastUDP recebe;
    private ComunicacaoUDP envia;
    
    public ChatController(String enderecoMulticast, int porta) throws UnknownHostException {
        this.recebe = new RecebedorMulticastUDP(enderecoMulticast, porta);
        this.envia = new ComunicacaoUDP(enderecoMulticast, porta);
    }
    
    public void iniciaChat() throws IOException {
        this.recebe.start();
        
        Scanner scan = new Scanner(System.in);
        System.out.print(">> ");
        
        while(true) {
            
            String msg = scan.nextLine();
            this.envia.enviaMensagem(msg);
        }
    }
}
