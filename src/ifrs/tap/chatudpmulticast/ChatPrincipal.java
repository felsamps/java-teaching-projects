package ifrs.tap.chatudpmulticast;

import java.io.IOException;

public class ChatPrincipal {
    public static void main(String[] args) {
        try {
            ChatController controller = new ChatController("230.0.0.4", 12345);
            controller.iniciaChat();
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }    
}
