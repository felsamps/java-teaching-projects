package ifrs.tap.chatudpmulticast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author fmsampaio
 */
public class ComunicacaoUDP {
    private DatagramSocket socket;
    private InetAddress group;
    private byte[] buff;
    private int porta;
    
    public ComunicacaoUDP(String enderecoMulticast, int porta) throws UnknownHostException {
        this.group = InetAddress.getByName(enderecoMulticast);
        this.porta = porta;
    }
    
    public void enviaMensagem(String mensagem) throws IOException {
        this.socket = new DatagramSocket();
        this.buff = mensagem.getBytes();
        
        DatagramPacket datagrama = new DatagramPacket(buff, buff.length, this.group, this.porta);
        this.socket.send(datagrama);
        socket.close();
    }
            
    
}
