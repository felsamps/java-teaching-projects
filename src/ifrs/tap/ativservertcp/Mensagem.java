package ifrs.tap.ativservertcp;

import java.net.InetAddress;
import java.util.ArrayList;


public class Mensagem {
    private InetAddress ip;
    private String mensagem, cmd;
    private ArrayList<String> parametros;
    
    public Mensagem(InetAddress ip, String msg) {
        this.ip = ip;
        this.mensagem = msg.trim().replace("\n","");
        String[] partes = mensagem.split(" ");
        
        this.cmd = partes[0];
        this.parametros = new ArrayList<>();
        
        for(int i = 1; i < partes.length; i++) {
        	this.parametros.add(partes[i]);
        }
    }
    
    public String getComando() {
        return this.cmd;
    }
    
    public String getParametro(int id) {
    	return this.parametros.get(id);
    }
    
    public String toString() {
        return this.ip + ": " + this.mensagem + "\n";
    }
}
