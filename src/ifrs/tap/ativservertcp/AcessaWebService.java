package ifrs.tap.ativservertcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;


public class AcessaWebService {
    public static JSONObject acessaHTTP(String urlString) throws MalformedURLException, JSONException, IOException {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.addRequestProperty("User-Agent", "Mozilla/4.76");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine, returnable = "";
        
        while ((inputLine = in.readLine()) != null)
            returnable += inputLine;
        
        in.close();
        
        return new JSONObject(returnable);        
    }
}
