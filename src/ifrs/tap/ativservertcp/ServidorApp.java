package ifrs.tap.ativservertcp;

import java.io.IOException;
import java.net.Socket;
import org.json.JSONException;




public class ServidorApp extends Thread {
    
    private static final String FINALIZA_CMD = "/sair";
    private static final String PESQUISA_CEP_CMD = "/cep";
    private static final String TEMPO_CMD = "/clima";
    private static final String DICIONARIO_CMD = "/dicionario";
    private static final String COTACAO_CMD = "/cotacao";
    private static final String ABOUT_CMD = "/sobre";
    
    private ServidorTCP servidorTCP;
    boolean finaliza;
    
    public ServidorApp(Socket socket) throws IOException {
        this.servidorTCP = new ServidorTCP(socket);
        this.finaliza = false;
    }
    
    public void run() {
        while(!this.finaliza) {
            try {
                Mensagem msgRecebida = this.servidorTCP.recebeMensagem();          
                this.trataMensagemCliente(msgRecebida);
                
            } 
            catch (IOException | JSONException ex) {
                ex.printStackTrace();
            }
            catch (NullPointerException npe) {
                System.out.println("Conexao com o cliente perdida");
                break;
            }
        }
    }
    
    private void trataMensagemCliente(Mensagem msgRecebida) throws IOException, JSONException {
        String comando = msgRecebida.getComando();
        
        System.out.println(msgRecebida);
            	
    	if(comando.equals(FINALIZA_CMD)) {
            this.finaliza = true;
            this.servidorTCP.enviaMensagem("Conexao finalizada");
            this.servidorTCP.encerraConexao();
        }
        else if(comando.equals(TEMPO_CMD)) {
            TrataTempo tempo = new TrataTempo();
            this.servidorTCP.enviaMensagem(tempo.tempoAtualFarroupilha() + "\n");
        }
        else if(comando.equals(DICIONARIO_CMD)) {
            String palavra = msgRecebida.getParametro(0);
            TrataDicionario dic = new TrataDicionario(palavra);
            for(String def : dic.getDifinicao()) {
                    this.servidorTCP.enviaMensagem(def + "\n");
            }
            this.servidorTCP.enviaMensagem("\n");
        	            
        }
        else if(comando.equals(COTACAO_CMD)) {
            TrataCotacao cot = new TrataCotacao();
            this.servidorTCP.enviaMensagem(cot.getCotacoes() + "\n");
        }
        else if(comando.equals(PESQUISA_CEP_CMD)) {
            String cep = msgRecebida.getParametro(0);
            TrataCEP consultaCEP = new TrataCEP(cep);
            this.servidorTCP.enviaMensagem(consultaCEP.consulta() + "\n");
        }
        else if(comando.equals(ABOUT_CMD)) {
            this.servidorTCP.enviaMensagem("Trabalho planejado para a disciplina de Tópicos Avançados de Programação\n\n");
        }
        else {
            this.servidorTCP.enviaMensagem("Comando não disponível\n\n");
        }
    }
    
    public static void main(String[] args) {
        
        
        
        try {
            ServidorTCP.iniciaServidor(12345);
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
        
        while(true) {
            try {
                Socket socket = ServidorTCP.geraNovaConexao();
                
                ServidorApp app = new ServidorApp(socket);
                app.start();
            }
            catch(IOException ioe) {
                ioe.printStackTrace();
            }
        }       
    }
    
}
