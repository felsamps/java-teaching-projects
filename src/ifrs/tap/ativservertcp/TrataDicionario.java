package ifrs.tap.ativservertcp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TrataDicionario {
	private static final String URL = "http://dicionario-aberto.net/search-json/";
	private String palavra, definicao;	
	
	public TrataDicionario(String palavra) {
		this.palavra = palavra;
	}
	
	public ArrayList<String> getDifinicao() throws MalformedURLException, JSONException, IOException {
		String urlBuscaDefinicao = URL + this.palavra;
		JSONObject retJson = AcessaWebService.acessaHTTP(urlBuscaDefinicao);
		
		JSONObject entry;
		
		if(retJson.has("superEntry")) {
			entry = retJson.getJSONArray("superEntry").getJSONObject(0).getJSONObject("entry");
		}
		else {
			entry = retJson.getJSONObject("entry");
		}
		
		JSONArray defs = entry.getJSONArray("sense");		
		
		ArrayList<String> returnable = new ArrayList<>();
		
		for(int i=0; i < defs.length(); i++) {
			JSONObject significado = defs.getJSONObject(i);
			returnable.add(significado.getString("def"));
		}
		
		return returnable;
	}
}
