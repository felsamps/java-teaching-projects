package ifrs.tap.ativservertcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorTCP {
    private static int portaServidor;
    private static ServerSocket socketServidor;
    
    private Socket socket;
    private BufferedReader entrada;
    private DataOutputStream saida;
    
    public static void iniciaServidor(int porta) throws IOException {
        portaServidor = porta;
        socketServidor = new ServerSocket(porta);
    }
    
    public static Socket geraNovaConexao() throws IOException {
        System.out.println("Aguardando conexões na porta " + portaServidor + " ...");
        Socket socket = socketServidor.accept();
        System.out.println("Conexão estabelecida com o cliente " + socket.getInetAddress());
        
        return socket;
    }
    
    public ServidorTCP(Socket socket) throws IOException {
        this.socket = socket;
        
        InputStream streamEntrada = this.socket.getInputStream();
        OutputStream streamSaida = this.socket.getOutputStream();

        this.entrada = new BufferedReader(new InputStreamReader(streamEntrada));
        this.saida = new DataOutputStream(streamSaida);
    }
    
    public Mensagem recebeMensagem() throws IOException {
        return new Mensagem(this.socket.getInetAddress(), this.entrada.readLine());
    }
    
    public void enviaMensagem(String msg) throws IOException {
        this.saida.writeBytes(msg);
    }
    
    public boolean clienteConectado() {
        return !this.socket.isClosed();
    }

    void encerraConexao() throws IOException {
        this.socket.close();
    }
}
