package ifrs.tap.ativservertcp;

import org.json.JSONArray;

import twitter4j.JSONException;
import twitter4j.JSONObject;

public class ManipulaJSON {
	public static final String formatoJson = 
			"{ " + 
			"	\"status\": true,\n" + 
			"	\"valores\": {\n" + 
			"		\"USD\": {\n" + 
			"			\"nome\": \"Dolar\",\n" + 
			"			\"valor\": 2.333,\n" + 
			"			\"ultima_consulta\": 1386349203,\n" + 
			"			\"fonte\": \"UOL Economia - http://economia.uol.com.br/cotacoes/\"\n" + 
			"		},\n" + 
			"		\"EUR\": {\n" + 
			"			\"nome\": \"Euro\",\n" + 
			"			\"valor\": 3.195,\n" + 
			"			\"ultima_consulta\": 1386349203,\n" + 
			"			\"fonte\": \"UOL Economia - http://economia.uol.com.br/cotacoes/\"\n" + 
			"		},\n" + 
			"		\"BTC\": {\n" + 
			"			\"nome\": \"Bitcoin\",\n" + 
			"			\"valor\": 2620,\n" + 
			"			\"ultima_consulta\": 1386352803,\n" + 
			"			\"fonte\": \"Mercado Bitcoin - http://www.mercadobitcoin.com.br/\"\n" + 
			"		}\n" + 
			"	}\n" + 
			"}";
	
	
	public static void main(String[] args) {
		try {
			JSONObject json = new JSONObject(formatoJson);
			
			System.out.println(json);
			
			JSONObject jsonValores = json.getJSONObject("valores");
			
			System.out.println(jsonValores);
			
			JSONObject jsonDolar = jsonValores.getJSONObject("USD");
			
			System.out.println(jsonDolar);
			
			String resumoCotacaoDolar = "";
			resumoCotacaoDolar += jsonDolar.getString("nome") + ": ";
			resumoCotacaoDolar += jsonDolar.getString("valor") + "\n";
			resumoCotacaoDolar += jsonDolar.getString("fonte") + "\n\n";
				
			System.out.println(resumoCotacaoDolar);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
