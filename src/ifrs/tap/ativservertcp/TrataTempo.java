
package ifrs.tap.ativservertcp;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class TrataTempo {
    //[{"id":5425,"name":"Farroupilha","state":"RS","country":"BR  "}]
    private static final String TOKEN = "df2e2d78cbbf8d5d56733169f75382c9";
    private static final String URL = "http://apiadvisor.climatempo.com.br/api/v1/weather/locale/#/current?token=";
    
    private Integer idCidade;
    
    public TrataTempo() {
        this.idCidade = 0;
    }
    
    public TrataTempo(String cidade, String estado) {
        //TODO implement city/state id search
        this.idCidade = 0;
    }
    
    public String tempoAtualFarroupilha() throws JSONException, IOException {
        this.idCidade = 5425;
        return this.acessaTempoAtual();
    }
    
    public String acessaTempoAtual() throws JSONException, IOException {
        String urlTempoFarroupilha = URL.replace("#", this.idCidade.toString()) + TOKEN;
        
        JSONObject retJSON = AcessaWebService.acessaHTTP(urlTempoFarroupilha);
        
        String cidade = retJSON.getString("name");
        String estado = retJSON.getString("state");
        
        String condicao = retJSON.getJSONObject("data").getString("condition");
        Integer temperatura = retJSON.getJSONObject("data").getInt("temperature");
        Integer umidade = retJSON.getJSONObject("data").getInt("humidity");
        
        String returnable = "Tempo em " + cidade + " " + estado + ":\n";
        returnable += condicao + "\n";
        returnable += "Temperatura: " + temperatura + "\n";
        returnable += "Umidade: " + umidade + "\n";
        
        return returnable;
    }
}
