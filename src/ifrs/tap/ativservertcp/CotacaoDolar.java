package ifrs.tap.ativservertcp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class CotacaoDolar {
    public static void main(String[] args) {
        
        try {
            URL url = new URL("https://api.promasters.net.br/cotacao/v1/valores");
            HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
            con.addRequestProperty("User-Agent", "Mozilla/4.76");
                    
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);
            in.close();
        }
        catch(Exception exc) {
            exc.printStackTrace();
        }
    }
}
