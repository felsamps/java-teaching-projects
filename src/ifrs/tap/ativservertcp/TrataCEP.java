package ifrs.tap.ativservertcp;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
public class TrataCEP {
    private final String URL = "http://viacep.com.br/ws/#/json";
    private String cep;
    
    public TrataCEP(String cep) {
        this.cep = cep;
    }
    
    public String consulta() throws JSONException, IOException {
        String urlPesquisa = URL.replace("#", cep);
        JSONObject retJSON = AcessaWebService.acessaHTTP(urlPesquisa);
        
        System.out.println(retJSON);
        
        String returnable = "";
            
        if(!retJSON.has("erro")) {
            returnable += "CEP: " + retJSON.getString("cep") + "\n";
            returnable += retJSON.getString("logradouro") + ", " + retJSON.getString("complemento") + "\n";
            returnable += retJSON.getString("bairro") + ", " + retJSON.getString("localidade") + " - " + 
                    retJSON.getString("uf") + "\n\n";
        }
        else {
            returnable += "CEP não encontrado!\n\n";
        }
        
        return returnable;
        
    }
}
