package ifrs.tap.ativservertcp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TrataCotacao {
    private final String URL_COTACOES = "https://api.promasters.net.br/cotacao/v1/valores";
    private final String URL_MOEDAS = "https://api.promasters.net.br/cotacao/v1/moedas";
    
    private JSONArray moedasDisponiveis;
    
    public TrataCotacao() throws JSONException, IOException {
        JSONObject retJSON = AcessaWebService.acessaHTTP(URL_MOEDAS);
        this.moedasDisponiveis = retJSON.getJSONObject("moedas").names();        
    }
    
    public String getCotacoes() throws JSONException, IOException {
        JSONObject retJSON = AcessaWebService.acessaHTTP(URL_COTACOES);
        String returnable = "";
        JSONObject cotacoes = retJSON.getJSONObject("valores");
        
        for (int i = 0; i < this.moedasDisponiveis.length(); i++) {
            JSONObject cotacao = cotacoes.getJSONObject(this.moedasDisponiveis.getString(i));
            
            returnable += cotacao.getString("nome") + ": ";
            returnable += cotacao.getDouble("valor") + "\n";
            
        }
        
        return returnable;
    }
}
